/*********************
*   PID.c
*   simple pid library 
*   pgaskell
**********************/

#include "PID.h"
#include <stdio.h>
#include <stdlib.h>


#define MAX_OUTPUT 1.0
#define MIN_OUTPUT -1.0
#define ITERM_MIN -1.0
#define ITERM_MAX 1.0

PID_t * PID_Init(float Kp, float Ki, float Kd) {
  // allocate memory for PID
  PID_t *pid =  malloc(sizeof(PID_t));

  //initalize values
  pid->pidInput = 0;
  pid->pidOutput = 0;
  pid->pidSetpoint = 0;
  pid->ITerm = 0;
  pid->prevInput = 0;

  // set update rate to 100000 us (0.1s)
  //IMPLEMENT ME!
  
  
  //set output limits, integral limits, tunings
  printf("initializing pid...\r\n");
  //IMPLEMENT ME!

  return pid;
}

void PID_Compute(PID_t* pid) {    
  // Compute PID 
  //IMPLEMENT ME!
}

void PID_SetTunings(PID_t* pid, float Kp, float Ki, float Kd) {
  //scale gains by update rate in seconds for proper units
  float updateRateInSec = ((float) pid->updateRate / 1000000.0);
  //set gains in PID struct
  //IMPLEMENT ME!
}

void PID_SetOutputLimits(PID_t* pid, float min, float max){
  //set output limits in PID struct
  //IMPLEMENT ME!
}

void PID_SetIntegralLimits(PID_t* pid, float min, float max){
  //set integral limits in PID struct
  //IMPLEMENT ME!

}

void PID_SetUpdateRate(PID_t* pid, int updateRate){
  //set integral limits in PID struct
  //IMPLEMENT ME!
}




